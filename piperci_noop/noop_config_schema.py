from marshmallow import fields, Schema, EXCLUDE


class NoOpConfigSchema(Schema):

    with_artifact = fields.Boolean(required=False,
                                   default=True,
                                   missing=True)

    class Meta:
        unknown = EXCLUDE
