## [1.0.1](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas/compare/v1.0.0...v1.0.1) (2019-12-03)


### Bug Fixes

* Change stack provider name ([5bf9944](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas/commit/5bf9944))
* Pass request json instead of task ([24de4de](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas/commit/24de4de))
* pin test dep ([d2f45e1](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas/commit/d2f45e1))

# 1.0.0 (2019-09-26)


### Bug Fixes

* Change gman/minio/func gateway hosts ([b3ceeca](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas/commit/b3ceeca))
