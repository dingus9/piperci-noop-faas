import json
import responses

from piperci.storeman.client import storage_client
from piperci.task.exceptions import PiperError, PiperDelegateError
from piperci.task.this_task import ThisTask

import pytest


@pytest.mark.parametrize("func_type", ["gateway", "executor"])
def test_no_json(client, func_type):
    headers = {"Content-Type": "application/json"}
    resp = client.post(f"/{func_type}", headers=headers)
    assert resp.status_code == 422


@pytest.mark.parametrize("func_type", ["gateway", "executor"])
def test_no_content_type_json(client, single_instance, func_type):
    resp = client.post(f"/{func_type}", data=str(single_instance))
    assert resp.status_code == 422


@responses.activate
def test_gateway(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_instance,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_noop.function.models.gman_client", gman_client_mock())
    resp = client.post("/gateway", json=single_instance, headers=common_headers)

    assert resp.status_code == 200


@responses.activate
def test_gateway_piper_error(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_instance,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    class CustTask(ThisTask):

        _init_storage_client = store_cli

        storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

        def info(self, *args, **kwargs):
            if args[0] == "Noop task gateway handler.handle called successfully":
                raise PiperError("Error for testing")
            return super().info(*args, **kwargs)

    mocker.patch("piperci_noop.function.faas_app.ThisTask", CustTask)
    mocker.patch("piperci_noop.function.models.gman_client", gman_client_mock())
    resp = client.post("/gateway", json=single_instance, headers=common_headers)

    assert resp.status_code == 400


@responses.activate
def test_gateway_piper_error_delegate(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    single_instance,
    start_task_response,
    task_artifact_upload_response,
    task_creation_response,
    task_post_executor_url_response
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(*task_post_executor_url_response[0])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
        storage_type="minio",
        hostname=minio_server,
        access_key="MINIO_TEST_ACCESS",
        secret_key="MINIO_TEST_SECRET",
    )

    class CustTask(ThisTask):

        _init_storage_client = store_cli

        storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

        def delegate(self, *args, **kwargs):
            raise PiperDelegateError("Error for testing")

    mocker.patch("piperci_noop.function.faas_app.ThisTask", CustTask)
    mocker.patch("piperci_noop.function.models.gman_client", gman_client_mock())
    resp = client.post("/gateway", json=single_instance, headers=common_headers)
    assert resp.status_code == 400


@responses.activate
@pytest.mark.parametrize("with_artifact", [True, False])
def test_executor(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_instance,
    with_artifact
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_noop.function.models.gman_client", gman_client_mock())
    threaded_instance["config"]["with_artifact"] = json.dumps(with_artifact)
    resp = client.post("/executor", json=threaded_instance, headers=common_headers)

    assert resp.status_code == 200


@responses.activate
def test_executor_piper_error(
    client,
    common_headers,
    minio_server,
    mocker,
    gman_client_mock,
    start_task_response,
    task_creation_response,
    task_artifact_upload_response,
    threaded_instance,
):
    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_creation_response[0], json=task_creation_response[1])
    responses.add(
        *task_artifact_upload_response[0], json=task_artifact_upload_response[1]
    )

    def store_cli(*args, **kwargs):
        return storage_client(  # noqa: S106
            storage_type="minio",
            hostname=minio_server,
            access_key="MINIO_TEST_ACCESS",
            secret_key="MINIO_TEST_SECRET",
        )

    def raise_exception(*args, **kwargs):
        raise PiperError("Error for testing")

    mocker.patch("piperci_noop.function.handler.execute_code", raise_exception)

    mocker.patch("piperci.task.this_task.ThisTask._init_storage_client", store_cli)
    mocker.patch("piperci_noop.function.models.gman_client", gman_client_mock())
    resp = client.post("/executor", json=threaded_instance, headers=common_headers)

    assert resp.status_code == 400
